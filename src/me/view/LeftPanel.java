package me.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import me.controller.RunActionListener;
import me.model.Model;

public class LeftPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3503493479850778530L;
	private Model model;
	private RightPanel right_panel;
	
	private JTextArea textArea;
	private JButton btnReset;
	private JButton btnRun;
	private JButton btnStop;

	/**
	 * Create the panel.
	 */
	public LeftPanel(int w, int h) {
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(w,h));
		setSize(w, h);

		System.out.println("lp " + getSize().toString());
		
		JPanel controls = new JPanel();
		controls.setLayout(new GridBagLayout());
		controls.setSize(getWidth(), (int)(getHeight()*0.3));
		add(controls, BorderLayout.NORTH);
		
		btnReset = new JButton();
		btnReset.setText("Reset field");
		GridBagConstraints gbc_reset = new GridBagConstraints();
		gbc_reset.gridx = 0;
		gbc_reset.gridy = 1;
		gbc_reset.anchor = GridBagConstraints.LINE_START;
		gbc_reset.insets = new Insets(5, 5, 5, 5);
		controls.add(btnReset, gbc_reset);
		
		btnRun = new JButton();
		btnRun.setText("Run program");
		GridBagConstraints gbc_run = new GridBagConstraints();
		gbc_run.gridx = 0;
		gbc_run.gridy = 2;
		gbc_run.anchor = GridBagConstraints.LINE_START;
		gbc_run.insets = new Insets(5, 5, 5, 5);
		controls.add(btnRun, gbc_run);
		
		btnStop = new JButton();
		btnStop.setText("Stop");
		GridBagConstraints gbc_stop = new GridBagConstraints();
		gbc_stop.gridx = 1;
		gbc_stop.gridy = 2;
		gbc_stop.anchor = GridBagConstraints.LINE_START;
		gbc_stop.insets = new Insets(5, 5, 5, 5);
		controls.add(btnStop, gbc_stop);
		
		textArea = new JTextArea(5, 20);
		textArea.setFont(new Font("Verdana", Font.BOLD, 14));
		JScrollPane scrollPane = new JScrollPane(textArea);
//		setPreferredSize(new Dimension(450, 110));
		add(scrollPane, BorderLayout.CENTER);
		
	}
	
	public int getCellSize() {
		return 25;
	}
	
	public int getLineWidth() {
		return 3;
	}
	
	public void setRightPanel(RightPanel p) {
		right_panel = p;
	}
	
	public void setModel(Model m) {
		model = m;
		btnRun.addActionListener(new RunActionListener(model, textArea));
		btnReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!model.isWaiting()) {
					model.clear();
					right_panel.repaint();
				}
			}
		});
		btnStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.stop();
			}
		});
	}
}
