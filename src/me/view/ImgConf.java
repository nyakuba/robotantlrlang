package me.view;

public class ImgConf {
	public final int cell_size;
	public final int line_width;
	public final int arc_width;
	public final int arc_height;
	
	public ImgConf(int cell_s, int line_w, int arc_w, int arc_h) {
		cell_size = cell_s;
		line_width = line_w;
		arc_width = arc_w;
		arc_height = arc_h;
	}
}
