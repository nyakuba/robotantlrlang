package me.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import me.controller.ImageMouseListener;
import me.model.Model;

public class Game extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2776272559265219968L;
	private JPanel contentPane;
	private LeftPanel leftPanel;
	private RightPanel rightPanel;
	private ImageMouseListener listener;
	private Model model;
	public final int initCellSize = 25;
	public final int initLineWidth = 3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Game frame = new Game();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Game() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Test ANTLR.");
		Dimension s_size = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0, 0, s_size.width, s_size.height);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setPreferredSize(new Dimension(s_size.width, 25));
		setJMenuBar(menuBar);

		JMenu mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);

		JMenuItem mntmGrammar = new JMenuItem("Grammar");
		mnMenu.add(mntmGrammar);
		mntmGrammar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFrame frame = new JFrame("Grammar");
				frame.setBounds(200, 200, 500, 500);
				frame.setVisible(true);
				frame.setLayout(new BorderLayout());
				JTextArea ta = new JTextArea(
						"command_list : command (SC command)* # commandList"
								+ ";"
								+ "\n"
								+ "command : /* epsilon */	# skip\n"
								+ "	| MOVE expr DIRECTION # move\n"
								+ "	| WHILE condition '{' command_list '}' # while\n"
								+ "	| IF condition '{' command_list '}' endif # if \n"
								+ "	| IDENT AS expr # assignment\n"
								+ "	;\n"
								+ "\n"
								+ "condition : expr op=( EQ | NE | GT | GE | LT | LE) expr # cond\n"
								+ "	;\n"
								+ "	\n"
								+ "endif : ELSE '{' command_list '}' # else\n"
								+ "	| /* empty */ # skip_else\n"
								+ "	;\n"
								+ "\n"
								+ "expr	: expr op=( PLUS | MINUS) mult # addRule\n"
								+ "	| mult # mul\n"
								+ "	;\n"
								+ "	\n"
								+ "mult	: mult op=( MULT | DIV ) factor # multRule\n"
								+ "	| factor # fact\n"
								+ "	;\n"
								+ "\n"
								+ "factor : NUMBER # int\n"
								+ "	| IDENT	# ident\n"
								+ "	| '(' expr ')' # parens\n"
								+ "	;\n"
								+ "	\n"
								+ "/*------------------------------------------------------------------\n"
								+ " * LEXER RULES\n"
								+ " *------------------------------------------------------------------*/\n"
								+ " \n"
								+ "PLUS : '+' ;\n"
								+ "MINUS : '-' ;\n"
								+ "MULT : '*' ;\n"
								+ "DIV : '/' ;\n"
								+ "SC : ';' ;\n"
								+ "AS : '=' ;\n"
								+ "EQ : '==' ;\n"
								+ "NE : '!=' ;\n"
								+ "GT : '>' ;\n"
								+ "GE : '>=' ;\n"
								+ "LT : '<' ;\n"
								+ "LE : '<=' ;\n"
								+ "MOVE : 'MOVE' | 'move' ;\n"
								+ "IF : 'IF' | 'if' ;\n"
								+ "ELSE : 'ELSE' | 'else' ;\n"
								+ "WHILE : 'WHILE' | 'while' ;\n"
								+ "DIRECTION : "
								+ "	| 'UP' | 'DOWN' | 'LEFT' | 'RIGHT' \n"
								+ "	| 'up' | 'down' | 'left' | 'right'\n"
								+ "	;\n"
								+ " \n"
								+ "NUMBER  : (DIGIT)+ ;\n"
								+ "IDENT : CHAR(DIGIT|CHAR)* ;\n"
								+ " \n"
								+ "WHITESPACE : ( '\\t' | ' ' | '\\r' | '\\n'| '\\u000C' )+ -> channel(HIDDEN);\n"
								+ " \n" + "fragment DIGIT  : '0'..'9' ;\n"
								+ "fragment CHAR : " + " | 'a'..'z'\n"
								+ " | 'A'..'Z'\n" + " | '$'\n" + " | '_'\n"
								+ " ;\n");
				ta.setBounds(10, 10, 480, 480);
				ta.setFont(new Font("Verdana", Font.BOLD, 14));
				ta.setEditable(false);
				JScrollPane scrollPane = new JScrollPane(ta);
				frame.add(scrollPane, BorderLayout.CENTER);
			}
		});

		JMenuItem mntmExit = new JMenuItem("Exit");
		mnMenu.add(mntmExit);
		mntmExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("exiting");
				System.exit(0);
			}
		});

		getContentPane().setLayout(null);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setPreferredSize(new Dimension(s_size.width,
				s_size.height - 35));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout());

		leftPanel = new LeftPanel((int) (s_size.width * 0.25),
				(int) (s_size.height * 0.9));
		leftPanel.setBounds(0, 0, (int) (s_size.width * 0.25),
				(int) (s_size.height * 0.9));
		contentPane.add(leftPanel, BorderLayout.WEST);

		createRightPanel();
	}

	public void createRightPanel() {
		Dimension s_size = Toolkit.getDefaultToolkit().getScreenSize();

		ImgConf config = new ImgConf(initCellSize, initLineWidth, 0, 0);
		Dimension rp_size = new Dimension((int) (s_size.width * 0.74),
				(int) (s_size.height * 0.9));
		int N = (rp_size.width - config.line_width) / config.cell_size;
		int M = (rp_size.height - config.line_width) / config.cell_size;
		if (N < 2)
			N = 2;
		if (M < 2)
			M = 2;
		model = new Model(N, M);

		if (rightPanel == null) {
			rightPanel = new RightPanel();
			rightPanel.setBounds(0, 0, rp_size.width, rp_size.height);
			listener = new ImageMouseListener();
			rightPanel.addMouseListener(listener);
			rightPanel.addMouseMotionListener(listener);

			contentPane.add(rightPanel, BorderLayout.CENTER);
			rightPanel.init();
		} else {
			listener.setModel(model);
		}

		rightPanel.setConfig(config);
		rightPanel.addModel(model);
		listener.setModel(model);
		leftPanel.setModel(model);
		leftPanel.setRightPanel(rightPanel);
	}
}
