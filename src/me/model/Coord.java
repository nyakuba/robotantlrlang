package me.model;

public class Coord {
	public int x;
	public int y;
	
	public Coord() {
		x = y = 0;
	}
	
	public Coord(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Coord(Coord c) {
		this.x = c.x;
		this.y = c.y;
	}
	
	public void setCoord(Coord c) {
		x = c.x;
		y = c.y;
	}
	
	public double length(Coord c) {
		return Math.sqrt(Math.pow(this.x - c.x, 2) + Math.pow(this.y - c.y, 2)); 
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == Coord.class) {
			Coord c = (Coord) obj;
			return this.x == c.x && this.y == c.y;
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public String toString() {
		return "Coord:["+x+","+y+"]";
	}
}
