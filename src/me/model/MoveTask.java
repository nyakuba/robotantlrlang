package me.model;

import java.util.TimerTask;

public class MoveTask extends TimerTask{
	private final Model model;
	private final Model.Direction d;
	
	public MoveTask(Model m, Model.Direction d) {
		this.model = m;
		this.d = d;
	}
	@Override
	public void run() {
		model.move(d);
		model.rmWaiting();
	}
}
