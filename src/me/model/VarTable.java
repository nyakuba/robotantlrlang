package me.model;

import java.util.HashMap;
import java.util.Map;

import me.exception.NoVariableException;

public class VarTable {
	private Map<String, Integer> table;
	
	public VarTable() { 
		table = new HashMap<String, Integer>();
	}
	
	public int getValue(String var_name) throws NoVariableException {
		if (table.containsKey(var_name)) {			
			return table.get(var_name);
		} else {
			throw new NoVariableException("no variable with name "+var_name);
		}
	}
	
	public void createIdent(String name, int value) {
		table.put(name, value);
	}
}
