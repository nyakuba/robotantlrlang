package me.model;

import java.util.Timer;

public class Model {
	private int N;
	private int M;
	private Cell field[][];
	private Robot robot;
	boolean editable;
	private int waiting;
	private int wcounter;
	private Timer t;
	private boolean finished;
	private boolean robotonfield;

	public enum Direction {
		UP, DOWN, LEFT, RIGHT;
	}

	public class Robot {
		public Coord coord;

		public Robot(int x, int y) {
			this.coord = new Coord(x, y);
		}
	}

	public Model(int n, int m) {
		editable = true;
		waiting = 0;
		wcounter = 0;
		finished = false;
		robotonfield = false;
		N = n;
		M = m;
		field = new Cell[N + 1][M + 1];
		for (int i = 0; i < N + 1; ++i)
			for (int j = 0; j < M + 1; ++j)
				field[i][j] = Cell.EMPTY;
		for (int i = 1; i < M - 1; ++i) {
			field[0][i] = Cell.WALL;
			field[N - 1][i] = Cell.WALL;
		}
		for (int i = 0; i < N; ++i) {
			field[i][0] = Cell.WALL;
			field[i][M - 1] = Cell.WALL;
		}
	}

	public void invertCell(int i, int j) {
		if (editable && waiting == 0) {
			if (i < N && j < M) {
				Cell cell = field[i][j];
				switch (cell) {
				case EMPTY:
					if (!robotonfield) {
						robot = new Robot(i, j);
						field[i][j] = Cell.ROBOT;
						robotonfield = true;
					} else {
						field[i][j] = Cell.STONE;
					}
					break;
				case STONE:
					field[i][j] = Cell.EMPTY;
					break;
				case ROBOT:
					robot = null;
					field[i][j] = Cell.EMPTY;
					robotonfield = false;
				default:
					break;
				}
			} else {
				System.err.println("invertCell : out of range i " + i + ", j "
						+ j);
			}
		} else {
			System.err.println("invertCell : model is not editable");
		}
	}

	public void invertCell(Coord c) {
		invertCell(c.x, c.y);
	}

	public void putRobot() {
		if (!robotonfield) {
			field[N/2][M/2] = Cell.ROBOT;
			robot = new Robot(N/2, M/2);
			robotonfield = true;
		}
	}
	
	public void clear() {
		if (editable) {
			for (int i = 2; i < N - 2; ++i) {
				for (int j = 1; j < M - 1; ++j) {
					field[i][j] = Cell.EMPTY;
				}
			}
			robot = null;
			robotonfield = false;
		} else {
			System.err.println("clear : model is not editable");
		}
	}

	public final Cell getCell(int i, int j) {
		if (i > N) {
			System.err.println("Model.getCell i: out of range");
			return Cell.EMPTY;
		}
		if (j > M) {
			System.err.println("Model.getCell j: out of range");
			return Cell.EMPTY;
		}
		return field[i][j];
	}

	public final Cell getCell(Coord c) {
		return getCell(c.x, c.y);
	}

	public int getN() {
		return N;
	}

	public int getM() {
		return M;
	}

	public void setEditable(boolean b) {
		editable = b;
	}

	public boolean isWaiting() {
		return waiting != 0;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setWaiting() {
		++waiting;
	}
	
	public void rmWaiting() {
		--waiting;
		if (waiting == 0) {
			wcounter = 0;
		}
	}

	public Coord getRobotCoord() {
		if (robot != null)
			return robot.coord;
		return null;
	}
	
	public void move(Direction d) {
		switch (d) {
		case UP:
			field[robot.coord.x][robot.coord.y] = Cell.EMPTY;
			--robot.coord.y;
			field[robot.coord.x][robot.coord.y] = Cell.ROBOT;
			break;
		case DOWN:
			field[robot.coord.x][robot.coord.y] = Cell.EMPTY;
			++robot.coord.y;
			field[robot.coord.x][robot.coord.y] = Cell.ROBOT;
			break;
		case LEFT: 
			field[robot.coord.x][robot.coord.y] = Cell.EMPTY;
			--robot.coord.x; 
			field[robot.coord.x][robot.coord.y] = Cell.ROBOT;
			break;
		case RIGHT: 
			field[robot.coord.x][robot.coord.y] = Cell.EMPTY;
			++robot.coord.x; 
			field[robot.coord.x][robot.coord.y] = Cell.ROBOT;
			break;
		}
	}
	
	public void move (int v, Direction d) {
		if (!isWaiting())
			t = new Timer();
		waiting += v;
		for (int i = 0; i < v; ++i) {
			t.schedule(new MoveTask(this, d), 1000*++wcounter);
		}
	}
	
	public void stop() {
		waiting = 0;
		wcounter = 0;
		try {
			t.cancel();
			t.purge();
		} catch (IllegalStateException ex) {}
	}
	
}
