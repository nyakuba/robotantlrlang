package me.exception;

public class NoVariableException extends Exception {
	private static final long serialVersionUID = 1462827788255584456L;
	public final String msg;
	public NoVariableException() {
		msg = "";
	}
	public NoVariableException(String str) {
		msg = str;
	}
}
