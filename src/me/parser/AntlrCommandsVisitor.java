package me.parser;

import me.ast.Node;
import me.exception.NoVariableException;
import me.model.Model;
import me.model.Model.Direction;
import me.model.VarTable;
import me.parser.CommandsParser.AddRuleContext;
import me.parser.CommandsParser.AssignmentContext;
import me.parser.CommandsParser.CommandListContext;
import me.parser.CommandsParser.CondContext;
import me.parser.CommandsParser.ElseContext;
import me.parser.CommandsParser.FactContext;
import me.parser.CommandsParser.IdentContext;
import me.parser.CommandsParser.IfContext;
import me.parser.CommandsParser.IntContext;
import me.parser.CommandsParser.MoveContext;
import me.parser.CommandsParser.MulContext;
import me.parser.CommandsParser.MultRuleContext;
import me.parser.CommandsParser.ParensContext;
import me.parser.CommandsParser.SkipContext;
import me.parser.CommandsParser.Skip_elseContext;
import me.parser.CommandsParser.WhileContext;

import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

public class AntlrCommandsVisitor implements CommandsVisitor<Node> {

	Model model;
	VarTable var_table;
	
	public AntlrCommandsVisitor(Model m) {
		model = m;
		var_table = new VarTable();
	}
	
	@Override
	public Node visit(ParseTree arg0) {
		if (arg0 == null) {
			return null;
		}
		System.out.println("visit "+ arg0.getText());
		if(MoveContext.class == arg0.getClass()) {
			return visitMove((MoveContext) arg0);
		} else if (IntContext.class == arg0.getClass()) {
			return visitInt((IntContext) arg0);
		} else if (CommandListContext.class == arg0.getClass()) {
			return visitCommandList((CommandListContext) arg0);
		} else if (AddRuleContext.class == arg0.getClass()) {
			return visitAddRule((AddRuleContext) arg0);
		} else if (MulContext.class == arg0.getClass()) {
			return visitMul((MulContext) arg0);
		} else if (MultRuleContext.class == arg0.getClass()) {
			return visitMultRule((MultRuleContext) arg0);
		} else if (SkipContext.class == arg0.getClass()) {
			return visitSkip((SkipContext) arg0);
		} else if (FactContext.class == arg0.getClass()) {
			return visitFact((FactContext) arg0);
		} else if (ParensContext.class == arg0.getClass()) {
			return visitParens((ParensContext) arg0);
		} else if (IdentContext.class == arg0.getClass()) {
			return visitIdent((IdentContext) arg0);
		} else if (AssignmentContext.class == arg0.getClass()) {
			return visitAssignment((AssignmentContext) arg0);
		} else if (CondContext.class == arg0.getClass()) {
			return visitCond((CondContext) arg0);
		} else if (WhileContext.class == arg0.getClass()) {
			return visitWhile((WhileContext) arg0);
		} else if (IfContext.class == arg0.getClass()) {
			return visitIf((IfContext) arg0);
		} else if (ElseContext.class == arg0.getClass()) {
			return visitElse((ElseContext) arg0);
		} else if (Skip_elseContext.class == arg0.getClass()) {
			return visitSkip_else((Skip_elseContext) arg0);
		} else {
			System.err.println("undefined rule context.");
			return null;
		}
	}

	@Override
	public Node visitChildren(RuleNode arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node visitErrorNode(ErrorNode arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node visitTerminal(TerminalNode arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node visitInt(IntContext ctx) {
		return new Node(Integer.valueOf(ctx.getText()));
	}

	@Override
	public Node visitCommandList(CommandListContext ctx) {
		int child_count = ctx.getChildCount();
		for (int i = 0; i < child_count; ++i) {
			ParseTree t = ctx.children.get(i);
			String txt = t.getText();
			System.out.println("txt "+txt);
			if (txt.compareTo(";") != 0 && txt != "") {
				visit(t);
			}
		}
		return null;
	}

	@Override
	public Node visitSkip(SkipContext ctx) {
		return null;
	}

	@Override
	public Node visitMove(MoveContext ctx) {
		System.out.println("move "+ ctx.getText());
		int value = visit(ctx.getChild(1)).integer_value;
		if (ctx != null) {
			String direction = ctx.getChild(2).getText();
			switch (direction) {
			case "up":
			case "UP": model.move(value, Direction.UP); break;
			case "down":
			case "DOWN": model.move(value, Direction.DOWN); break;
			case "left":
			case "LEFT": model.move(value, Direction.LEFT); break;
			case "right":
			case "RIGHT": model.move(value, Direction.RIGHT); break;
			default: break;
			}
		}
		return null;
	}

	@Override
	public Node visitAddRule(AddRuleContext ctx) {
		switch (ctx.op.getText()) {
		case "+" : 
			return new Node(visit(ctx.getChild(0)).integer_value + visit(ctx.getChild(2)).integer_value);
		case "-" : 
			return new Node(visit(ctx.getChild(0)).integer_value - visit(ctx.getChild(2)).integer_value);
		default : System.err.println("invalid addop");
		}
		return null;
	}

	@Override
	public Node visitFact(FactContext ctx) {
		return visit(ctx.getChild(0));
	}

	@Override
	public Node visitParens(ParensContext ctx) {
		return visit(ctx.getChild(1));
	}

	@Override
	public Node visitMultRule(MultRuleContext ctx) {
		switch (ctx.op.getText()) {
		case "*" : 
			return new Node(visit(ctx.getChild(0)).integer_value * visit(ctx.getChild(2)).integer_value);
		case "/" :
			int denom = visit(ctx.getChild(2)).integer_value;
			if (denom != 0)
				return new Node(visit(ctx.getChild(0)).integer_value / denom);
			else {
				System.err.println("division by zero");
				return null;
			}
		default : System.err.println("invalid mulop");
		}
		return null;
	}

	@Override
	public Node visitMul(MulContext ctx) {
		return visit(ctx.getChild(0));
	}

	@Override
	public Node visitAssignment(AssignmentContext ctx) {
		System.out.println("ident rule :"+ctx.getText());
		System.out.println("ident rule :"+ctx.getChild(0).getText());
		var_table.createIdent(ctx.getChild(0).getText(), visit(ctx.getChild(2)).integer_value);
		return null;
	}

	@Override
	public Node visitIdent(IdentContext ctx) {
		Node node = null;
		try {
			node = new Node(var_table.getValue(ctx.getText()));
		} catch (NoVariableException ex) {
			System.err.println(ex.msg);
		}
		return node;
	}

	@Override
	public Node visitCond(CondContext ctx) {
		switch (ctx.getChild(1).getText()) {
		case "==":
			return new Node(visit(ctx.getChild(0)).integer_value == visit(ctx.getChild(2)).integer_value);
		case "!=":
			return new Node(visit(ctx.getChild(0)).integer_value != visit(ctx.getChild(2)).integer_value);
		case ">":
			return new Node(visit(ctx.getChild(0)).integer_value > visit(ctx.getChild(2)).integer_value);
		case ">=":
			return new Node(visit(ctx.getChild(0)).integer_value >= visit(ctx.getChild(2)).integer_value);
		case "<":
			return new Node(visit(ctx.getChild(0)).integer_value < visit(ctx.getChild(2)).integer_value);
		case "<=":
			return new Node(visit(ctx.getChild(0)).integer_value <= visit(ctx.getChild(2)).integer_value);
		default: 
			System.err.println("no relation "+ctx.getChild(1).getText()+" exists");
		}
		return null;
	}

	@Override
	public Node visitWhile(WhileContext ctx) {
		while (visit(ctx.getChild(1)).boolean_value) {
			visit(ctx.getChild(3));
		}
		return null;
	}

	@Override
	public Node visitElse(ElseContext ctx) {
		return visit(ctx.getChild(2));
	}

	@Override
	public Node visitIf(IfContext ctx) {
		if (visit(ctx.getChild(1)).boolean_value) {
			visit(ctx.getChild(3));
		} else {
			visit(ctx.getChild(5));
		}
		return null;
	}

	@Override
	public Node visitSkip_else(Skip_elseContext ctx) {
		return null;
	}
	
}
