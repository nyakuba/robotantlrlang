grammar Commands;
 
@header {
//package me.parser;
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

command_list 	: command (SC command)* 						# commandList
				;

command 		: /* epsilon */									# skip
				| MOVE expr DIRECTION							# move
				| WHILE condition '{' command_list '}' 			# while
				| IF condition '{' command_list '}' endif		# if 
				| IDENT AS expr									# assignment
				;

condition		: expr op=( EQ | NE | GT | GE | LT | LE) expr	# cond
				;
				
endif			: ELSE '{' command_list '}'						# else
				| /* empty */									# skip_else
				;

expr			: expr op=( PLUS | MINUS) mult					# addRule
				| mult											# mul
				;
				
mult			: mult op=( MULT | DIV ) factor					# multRule
				| factor										# fact
				;

factor			: NUMBER										# int
				| IDENT											# ident
				| '(' expr ')'									# parens
				;
				 
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/
 
PLUS : '+' ;
MINUS : '-' ;
MULT : '*' ;
DIV : '/' ;
SC : ';' ;
AS : '=' ;
EQ : '==' ;
NE : '!=' ;
GT : '>' ;
GE : '>=' ;
LT : '<' ;
LE : '<=' ;
MOVE : 'MOVE' | 'move' ;
IF : 'IF' | 'if' ;
ELSE : 'ELSE' | 'else' ;
WHILE : 'WHILE' | 'while' ;
DIRECTION : 'UP' | 'DOWN' | 'LEFT' | 'RIGHT' 
		  | 'up' | 'down' | 'left' | 'right'
		  ;
 
NUMBER  : (DIGIT)+ ;
IDENT	: CHAR(DIGIT|CHAR)* ;
 
WHITESPACE : ( '\t' | ' ' | '\r' | '\n'| '\u000C' )+ -> channel(HIDDEN);
 
fragment DIGIT  : '0'..'9' ;
fragment CHAR	: 'a'..'z'
				| 'A'..'Z' 
				| '$'
				| '_'
				;
