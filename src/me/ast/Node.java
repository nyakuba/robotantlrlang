package me.ast;

public class Node {
	public boolean boolean_value;
	public int integer_value;
	public String string_value;
	public Node(int value) {
		integer_value = value;
	}
	public Node(String str) {
		string_value = str;
	}
	public Node(boolean b) {
		boolean_value = b;
	}
}
