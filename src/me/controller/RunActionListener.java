package me.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

import me.model.Model;
import me.parser.AntlrCommandsVisitor;
import me.parser.CommandsLexer;
import me.parser.CommandsParser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenSource;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class RunActionListener implements ActionListener {
	
	private Model model;
	private JTextArea textArea;
	
	public RunActionListener(Model m, JTextArea ta) {
		model = m;
		textArea = ta;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (!model.isWaiting()) {
			model.putRobot();
			CharStream cs = new ANTLRInputStream(textArea.getText());
			CommandsLexer lexer = new CommandsLexer(cs);
			TokenStream tokens = new CommonTokenStream((TokenSource) lexer);
			CommandsParser parser = new CommandsParser(tokens);
	
			AntlrCommandsVisitor visitor = new AntlrCommandsVisitor(model);
			ParseTree t = parser.command_list();
			t.accept(visitor);
		}
	}
}
