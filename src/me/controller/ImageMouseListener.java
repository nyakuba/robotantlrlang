package me.controller;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import me.model.Cell;
import me.model.Coord;
import me.model.Model;
import me.view.RightPanel;

public class ImageMouseListener implements MouseListener, MouseMotionListener{
	private Model model;
	private Coord currentCell;
	private Coord lastCell;
	private boolean mousePressed;
	private Cell initDragCell;

	public ImageMouseListener() {}
	
	public void setModel(Model m) {
		model = m;
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
//		System.out.println("Mouse dragged "+e.getPoint().toString());
		RightPanel rp = (RightPanel) e.getSource();
		Coord cCell = point2coord(e.getPoint(), rp.getConfig().cell_size);
		if (currentCell != null) {
			if (!cCell.equals(currentCell)) {
				switch (initDragCell) {
				case EMPTY:
					if (model.getCell(cCell) == Cell.EMPTY)
						model.invertCell(cCell);
					break;
				case STONE:
					if (model.getCell(cCell) == Cell.STONE)
						model.invertCell(cCell);
					break;
				default : break;
				}
				lastCell = currentCell;
				currentCell.setCoord(cCell);
				rp.repaint();
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
//		System.out.println("Mouse moved "+e.getPoint().toString());
		RightPanel rp = (RightPanel) e.getSource();
		Coord cCell = point2coord(e.getPoint(), rp.getConfig().cell_size);
		if (!cCell.equals(currentCell)) {
			lastCell = currentCell;
			currentCell.setCoord(cCell);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
//		System.out.println("Mouse clicked "+e.getPoint().toString());
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		lastCell = null;
		RightPanel rp = (RightPanel) e.getSource();
		currentCell = point2coord(e.getPoint(), rp.getConfig().cell_size);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		currentCell = null;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		System.out.println("Mouse pressed "+e.getPoint().toString());
		mousePressed = true;
		RightPanel rp = (RightPanel) e.getSource();
		Coord cCell = point2coord(e.getPoint(), rp.getConfig().cell_size);
		initDragCell = model.getCell(cCell);
		model.invertCell(currentCell.x, currentCell.y);
		rp.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
//		System.out.println("Mouse released "+e.getPoint().toString());
		mousePressed = false;
		initDragCell = null;
	}
	
	private Coord point2coord(Point p, int cell_size) {
		int i = (int) Math.ceil(p.x / cell_size);
		int j = (int) Math.ceil(p.y / cell_size);	
		return new Coord(i, j);
	}
	
}
